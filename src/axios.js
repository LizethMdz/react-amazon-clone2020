import axios from 'axios';

const instance = axios.create({
    // only works in localhost :(
    baseURL: 'http://localhost:5001/clone-app-3e264/us-central1/api' //THE API {CLOUD FUNCTION} url
});

export default instance;