import firebase from "firebase";

const firebaseConfig = {
  apiKey: "AIzaSyCS1TsuBuxVwS4Shw7octsq_knCpZTTuxA",
  authDomain: "clone-app-3e264.firebaseapp.com",
  databaseURL: "https://clone-app-3e264.firebaseio.com",
  projectId: "clone-app-3e264",
  storageBucket: "clone-app-3e264.appspot.com",
  messagingSenderId: "154239637987",
  appId: "1:154239637987:web:cf2a500c768477cedf28d6"
};

const firebaseApp = firebase.initializeApp(firebaseConfig);

const db = firebaseApp.firestore();
const auth = firebase.auth();

export { db, auth };
